import sys
import time
from os import sep

import serial
from serial.serialutil import SerialException

from tkinter import messagebox

"""
Return codes
-1  Timeout
 0  OK
 1  General error
"""

VERBOSE_OUTPUT = False
INIT_SLEEP_S = 1.6
BAUD = 115200
MODE_READ = 'AA'
MODE_RECEIVE = 'DD'
MODE_WRITE = 'AD'
MODE_ERASE = '7D'


class ArduinoEEPROM():
    def __init__(self, serial_address=None):
        self.verbose = False
        self.addr = serial_address
        if serial_address is None:
            self.serial_instance = None
        else:
            self.set_serial_instance(serial_address)

    def set_serial_instance(self, address):
        self.addr = address
        try:
            self.serial_instance = serial.Serial(port=address, baudrate=BAUD, timeout=5)
        except SerialException as e:
            print(e)
            messagebox.showerror(title='Serial Error', message=e)
            self.serial_instance = None
        else:
            print(f'Connected to {address}')
            self.serial_instance.close()

    def send_buffer(self, buffer):
        print('Sending data to Arduino ', end='')
        size_of_buf = len(buffer)
        i = 0
        # pgb_list = ['-' for x in range(48)]
        # pgb_scale = size_of_buf / len(pgb_list)
        while i < size_of_buf:
            if self.verbose:
                print('#%d: %.2x%.2x' % (i / 2, buffer[i], buffer[i + 1]))
            else:
                pass
                # print_pg_bar(pgb_list)
                # pgb_list[int(addr / pgb_scale)] = '+'
            self.serial_instance.write(buffer[i].to_bytes(1, byteorder='big'))
            self.serial_instance.write(buffer[i + 1].to_bytes(1, byteorder='big'))
            self.serial_instance.flush()

            response = self.serial_instance.read(3)
            if self.verbose: print('->%s' % response)
            if response != b'OK!':
                print('Response error')
                break
            i = i + 2
        print('')

    def get_data_from_programmer(self):
        if not self.serial_instance.isOpen():
            self.serial_instance.open()
        print('Receiving data from Arduino')
        in_buffer = self.serial_instance.read_all()
        message_len = len(in_buffer)
        if self.verbose: print('Incoming %d B' % (message_len))
        return in_buffer

    def write_eeprom(self, input_data):
        print('== Write EEPROM ==')
        try:
            if not self.serial_instance.isOpen():
                self.serial_instance.open()
                time.sleep(INIT_SLEEP_S)
            file_buffer = input_data
            print('Set programmer mode:' + MODE_RECEIVE)
            self.serial_instance.write(bytearray.fromhex(MODE_RECEIVE))  # Prepare programmer to receive data from pc
            self.serial_instance.flush()
            prog_resp = self.serial_instance.readline()
            print('Arduino->' + prog_resp.decode())
            if len(prog_resp) == 0:
                print('No response form arduino')
                return -1
            self.send_buffer(file_buffer)
            self.serial_instance.reset_input_buffer()
            time.sleep(2)
            # Read arduino cache
            buffer = self.get_data_from_programmer()
            if buffer == file_buffer: # Data is intact
                print('Memory compare - OK!')
                print('Set programmer mode:' + MODE_WRITE)
                self.serial_instance.write(bytearray.fromhex(MODE_WRITE))  # Write received data to EEPROM
                prog_resp = self.serial_instance.readline()
                print('Arduino->' + prog_resp.decode(), end='')
                # self.serial_instance.close()
                return 0
            else: # Bad serial connection
                print('Memory compare - FAIL')
                self.serial_instance.close()
                return 1

        except serial.SerialException as e:
            print('SerialException', e)
            return 1

    def read_eeprom(self, file_write_enable=True):
        print('== Read EEPROM ==')
        out_file = '.%sdump_%s.bin' % (sep, time.strftime("%Y-%m-%d-%H%M%S", time.localtime()))
        try:
            if not self.serial_instance.isOpen():
                self.serial_instance.open()
                time.sleep(INIT_SLEEP_S)
            self.serial_instance.reset_output_buffer()
            self.serial_instance.reset_input_buffer()
            print('Set programmer mode:' + MODE_READ)
            self.serial_instance.write(bytearray.fromhex(MODE_READ))
            prog_resp = self.serial_instance.readline()
            print('Arduino->' + prog_resp.decode())
            if len(prog_resp) == 0:
                print('No response form arduino')
                return b''
            time.sleep(0.5)
            buffer = self.get_data_from_programmer()
            self.serial_instance.close()
            if len(buffer) > 0:
                if file_write_enable:
                    write_to_file(out_file, buffer)
                return buffer
            else:
                print('No response')
                return b''
        except serial.SerialException as e:
            print('SerialException', e)

    def arduino_init(self, timeout):
        print('Checking arduino:', end=' ')
        try:
            if not self.serial_instance.isOpen():
                self.serial_instance.open()
                time.sleep(INIT_SLEEP_S)
            test_msg = 'OK?\n'
            resp_msg = ''
            delay = 0.1
            timeout = timeout / delay
            self.serial_instance.reset_output_buffer()
            self.serial_instance.reset_input_buffer()
            while (resp_msg != test_msg) and (timeout > 0):
                self.serial_instance.write(test_msg.encode())
                if self.serial_instance.in_waiting > 0:
                    resp_msg = self.serial_instance.readline().decode()
                time.sleep(delay)
                timeout = timeout - 1
            self.serial_instance.close()
            if timeout <= 0:
                return 'Arduino timeout'
            else:
                return 'Arduino is ready'
            # self.serial_instance.reset_output_buffer()
            # self.serial_instance.reset_input_buffer()
        except serial.SerialException as e:
            print('SerialException', e)

    def erase_eeprom(self, completion_check=True):
        print('== Erase EEPROM ==')
        try:
            if not self.serial_instance.isOpen():
                self.serial_instance.open()
                time.sleep(INIT_SLEEP_S)
            print('Set programmer mode:' + MODE_ERASE)
            self.serial_instance.write(bytearray.fromhex(MODE_ERASE))
            self.serial_instance.flush()
            prog_resp = self.serial_instance.readline()
            print('Arduino->' + prog_resp.decode())
            if len(prog_resp) == 0:
                print('No response form arduino')
                return -1
            else:
                if completion_check:  # ERASE Check. Look for anything other than 0xFF
                    time.sleep(1)
                    res = self.check_all_values_are_equal_to('ff')
                    if res == 0:
                        print('Erase successful')
                        return 0
                    else:
                        print('Erase unsuccessful')
                        return 1
                else:  # Don't check ERASE completion
                    print('Erase All executed')
                return 'Erase All executed'
        except serial.SerialException as e:
            print('SerialException', e)

    def check_all_values_are_equal_to(self, hex_str):
        read_back_string = self.read_eeprom(False).hex()
        i = 0
        while i <= len(read_back_string) - 1:
            current_chars = read_back_string[i:i + 2]
            if current_chars != hex_str:
                print('%d: %s is not %s ' % (i, current_chars, hex_str))
                return 1
            i += 2
        return 0

    def write_and_compare(self, binary_array):
        call_result = self.write_eeprom(binary_array)
        if call_result == 0:
            time.sleep(2)
            print('\nVerifying data')
            read_back = self.read_eeprom(file_write_enable=False)
            if binary_array == read_back:
                print('Content verification OK!')
                return 'Content verification OK!'
            else:
                print('Content verification error')
                return 'Content verification error'
        else:
            print('Data transfer error: %d'%call_result)
            return ('Data transfer error: %d'%call_result)

    def write_handler(self, binary_file):
        # self.set_serial_instance(port)
        self.serial_instance.open()
        time.sleep(INIT_SLEEP_S)
        return self.write_and_compare(binary_file.buffer)
        # init_result = self.arduino_init(5)
        # print(init_result)

        # if init_result == 0:
        #     return self.write_and_compare(binary_file.buffer)
        # elif init_result == -1:
        #     return 'Arduino timed out'

    def read_handler(self):
        # serial_port = serial.Serial(port=self.get_combobox_item(), baudrate=BAUD, timeout=5)
        return self.read_eeprom()

    def read_test_handler(self):
        read_message = self.read_eeprom(False)
        self.print_long_string(read_message.hex())
        return read_message

    def erase_handler(self):
        call_result = self.erase_eeprom()
        # serial_port.close()
        if call_result == 0:
            return 'Erase completed'
        else:
            return 'Erase unsuccessful: %d'%call_result

    @staticmethod
    def print_long_string(long_str):
        line = ''
        for char in long_str:
            if (len(line) % (64 + 1)) == 0:  # 64 chars + \n
                line += '\n'
            line += char
        print(line)


def print_pg_bar(pg_bar):
    result_list = ['['] + pg_bar + [']']
    print('', end='\r')
    sys.stdout.write(''.join(result_list))
    sys.stdout.flush()


def write_to_file(filename, buffer):
    print(f'Writing {len(buffer)}B to file {filename}')
    with open(filename, 'wb') as dump_file:
        dump_file.write(buffer)
