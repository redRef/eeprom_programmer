from os.path import isfile

# Head unit eeprom
EXPECTED_SIZE = 256
CHECKSUM_RANGE_START = 0x1C
CHECKSUM_RANGE_END = 0xFD
CHECKSUM_OFFSET = 0xFE
CHECKSUM_SIGNATURE = 0xA66A
CHECKSUM_METHOD = 'xor'
VIN_ADDRS_A = [0x87, 0x86, 0x89]
VIN_ADDRS_B = [0x97, 0x96, 0x99]

# CD changer eeprom
# EXPECTED_SIZE = 256
# CHECKSUM_RANGE_START = 0x00
# CHECKSUM_RANGE_END = 0x1A
# CHECKSUM_OFFSET = 0x1A
# CHECKSUM_SIGNATURE = 0x6AA6
# CHECKSUM_METHOD = 'sum'
# VIN_ADDRS_A = [0x13, 0x14, 0x15]
# VIN_ADDRS_B = [0x13, 0x14, 0x15]


class BinaryFile:
    def __init__(self, filename):
        if not isfile(filename):
            raise IOError('"%s" is not a file' % filename)
        print('Reading file: %s' % filename)
        with open(filename, 'rb') as f:     # Mode: read, binary
            self.buffer = bytearray(f.read())
            if self.get_size() != EXPECTED_SIZE:
                raise IOError('Unexpected file size (expected:%d, actual:%d)' % (EXPECTED_SIZE, self.get_size()))
            if self.get_word(CHECKSUM_OFFSET-2) == 0x6AA6:
                raise IOError('Little-endian byte order is not supported')
        self.print_info()

    def print_info(self):
        checksum_calculated = self.calc_checksum(method=CHECKSUM_METHOD)
        checksum_read = self.read_checksum()
        if checksum_read != checksum_calculated:
            print('** WARNING ** Bad checksum (expected:%04X, actual:%04X)' % (checksum_read, checksum_calculated))
        vin_a, vin_b = self.get_vin(True)
        print('[SIZE=%dB; CHECKSUM=%04X/%04X; VIN=%s/%s]' %
              (self.get_size(), checksum_read, checksum_calculated, vin_a, vin_b))

    def get_size(self):
        return len(self.buffer)

    def get_byte(self, addr):
        return self.buffer[addr]

    def get_word(self, addr):
        return self.get_byte(addr) << 8 | self.get_byte(addr + 1)

    def read_checksum(self):
        return self.get_word(CHECKSUM_OFFSET)
        # return int(self.buffer[CHECKSUM_OFFSET] << 8 | self.buffer[CHECKSUM_OFFSET+1])

    def put_checksum(self, new_checksum):
        print("%04X"%new_checksum)
        self.buffer[CHECKSUM_OFFSET], self.buffer[CHECKSUM_OFFSET+1] = new_checksum.to_bytes(2, byteorder='big')
        print('%04X %04X'%(self.buffer[CHECKSUM_OFFSET], self.buffer[CHECKSUM_OFFSET+1]))

    def put_byte(self, offset, data):
        if 0 <= offset < EXPECTED_SIZE:
            self.buffer[offset] = data
            return 0
        else:
            print('Offset %X is out of range'%offset)
            return -1

    def calc_checksum(self, start=CHECKSUM_RANGE_START, end=CHECKSUM_RANGE_END, print_progress=False, method='xor'):
        checksum = 0
        cursor = start
        if print_progress:
            print('Checksum calculation...')
        while cursor < end:
            next_word = int(self.buffer[cursor + 1] | self.buffer[cursor] << 8)
            # next_word = self.buffer[cursor + 1] + self.buffer[cursor]
            prev_checksum = checksum
            if method == 'xor':
                checksum ^= next_word
            elif method == 'sum':
                checksum += self.buffer[cursor + 1] + self.buffer[cursor]
                # checksum += int(self.buffer[cursor + 1])
                # print(f"masked {checksum & 0xFFFF:04X}")
            if print_progress:
                print(f"{cursor:04X}: {prev_checksum:04X} {method} {next_word:04X} = {checksum:04X}")
                # print('%04X: %04X ^ %04X = %04X' % (i, prev_checksum, next_word, checksum))
            cursor += 2
        return checksum

    def __str__(self):
        return self.buffer.hex()

    def __repr__(self):
        return self.buffer.hex()

    def print_file_in_hex(self):
        string = self.buffer.hex()
        print('='*25)
        print(string)
        self.calc_checksum(method=CHECKSUM_METHOD)
        print('Checksum from file: %s' % string[-4:])

    def get_vin(self, print_warning=False):
        # Assumption:
        # if A != B -> RADIO LOCKED
        # sometimes A is rubbish or 0xFF, but B is okay
        # sometimes it's the other way around
        vin_a = bytearray([])
        vin_b = bytearray([])
        for address in VIN_ADDRS_A:
            vin_a.append(self.get_byte(address))
        for address in VIN_ADDRS_B:
            vin_b.append(self.get_byte(address))
        vin_a = ShiftString(vin_a.hex())
        vin_b = ShiftString(vin_b.hex())
        vin_a = vin_a << 1
        vin_b = vin_b << 1
        if (vin_a != vin_b) and print_warning:
            print('** WARNING ** Two VINs found (one:%s, two:%s)' % (vin_a, vin_b))
        return [vin_a, vin_b]

    def put_vin(self, vin):
        if len(vin) > 6:
            vin = vin[-6:]
        if vin.isdigit() and (len(vin) == 6):
            vin = ShiftString(vin)
            vin = vin >> 1
            vin_splitted = [vin[0:2], vin[2:4], vin[4:6]]
            print(vin_splitted)
            for index, address in enumerate(VIN_ADDRS_A):
                self.put_byte(address, int(vin_splitted[index], 16))
            for index, address in enumerate(VIN_ADDRS_B):
                self.put_byte(address, int(vin_splitted[index], 16))
            self.put_checksum(self.calc_checksum(print_progress=True, method=CHECKSUM_METHOD))
        else:
            print('Enter VIN\'s last 6 digits. This part contains only digits')

    def write_to_file(self, filename):
        with open(filename, 'wb') as f:     # Mode: write, binary
            f.write(self.buffer)


class ShiftString(str):
    def __lshift__(self, num):
        return self[num:] + self[:num]

    def __rshift__(self, num):
        return self[-num:] + self[:-num]


# # k = BinaryFile('./EEPROM_dumps/binary_test.bin')
# k = BinaryFile('./EEPROM_dumps/my_dump_test_arduino_array.bin')
# k.put_vin('056019')
# k.put_checksum(k.calc_checksum())
# k.write_to_file('./EEPROM_dumps/write_test_0.bin')
# k = BinaryFile('./EEPROM_dumps/write_test_0.bin')
