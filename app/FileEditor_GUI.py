import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from os import sep
from serial.tools import list_ports
from BinaryFile import BinaryFile
from ArduinoEEPROM import ArduinoEEPROM

class FileEditor(ttk.Frame):
    def __init__(self, master=None):
        self.filename = ''
        self.binary_file = None
        ttk.Frame.__init__(self, master)
        self.grid(padx=10, pady=10)
        self.filename_text = tk.StringVar()
        self.filename_text.set("Select File")
        self.vin_text = tk.StringVar()
        self.vin_text.set("Last 6 digits of VIN")
        self.checksum_text = tk.StringVar()
        self.create_widgets()
        self.master.resizable(0, 0)
        self.master.title('File Editor')
        self.style = ttk.Style()
        self.style.theme_use("clam")


    def create_widgets(self):
        # File frame
        self.file_frame = ttk.LabelFrame(self, text='File')
        self.file_frame.grid(row=0, column=0, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.entry = ttk.Entry(self.file_frame, textvariable=self.filename_text)
        self.browse_button = ttk.Button(self.file_frame, text="Browse", command=self.browse_handler)
        self.print_button = ttk.Button(self.file_frame, text="Print to console", command=self.print_handler)

        self.entry.grid(row=1, column=0, sticky="nsew", padx=2, pady=2)
        self.browse_button.grid(row=1, column=1, sticky="nsew", padx=2, pady=2)
        self.print_button.grid(row=2, column=0, columnspan=2, sticky="nsew", padx=2, pady=2)

        # File frame
        self.file_frame = ttk.LabelFrame(self, text='File')
        self.file_frame.grid(row=1, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.vin_entry = ttk.Entry(self.file_frame, textvariable=self.vin_text)
        self.vin_button = ttk.Button(self.file_frame, text="Apply VIN", command=self.vin_handler)
        self.write_button = ttk.Button(self.file_frame, text="Write File", command=self.write_handler)

        self.vin_entry.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)
        self.vin_button.grid(row=0, column=1, sticky="nsew", padx=2, pady=2)
        self.write_button.grid(row=99, column=0, columnspan=2, sticky="nsew", padx=2, pady=2)

        # Checksum frame
        self.checksum_frame = ttk.LabelFrame(self, text='Checksum')
        self.checksum_frame.grid(row=2, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.text_field = ttk.Label(self.checksum_frame, textvariable=self.checksum_text)
        self.text_field.grid(sticky="nsew", padx=2, pady=2)

    def get_file(self):
        return self.binary_file

    def set_file(self, new_filename):
        try:
            self.binary_file = BinaryFile(new_filename)
        except IOError as e:
            print('IOError', e)
            self.binary_file = None
            self.set_entry_text('')
            self.set_message(str(e))
        else:
            self.set_entry_text(new_filename)
            self.set_message(self.get_checksum_str())

    def set_programmer(self, portname):
        if self.programmer is None:
            self.programmer = ArduinoEEPROM(portname)
        else:
            self.programmer.set_serial_instance(portname)

    def set_entry_text(self, new_text):
        self.filename_text.set(new_text.split(sep)[-1])

    def set_message(self, new_message):
        if type(new_message) is not str:
            new_message = str(new_message)
        self.checksum_text.set(new_message)

    def get_checksum_str(self):
        vin_a, vin_b = self.binary_file.get_vin()
        cs_a = self.binary_file.read_checksum()
        cs_b = self.binary_file.calc_checksum()
        return 'CHKSUM:%04X/%04X  VIN:%s/%s ' % (cs_a, cs_b, vin_a, vin_b)

    def browse_handler(self):
        filename = filedialog.askopenfilename(filetypes=(("Binary file", "*.bin"),("All files","*.*")))
        if len(filename) > 0:
            # self.set_filename(filename)
            self.set_file(filename)

    def write_handler(self):
        raise NotImplementedError

    def print_handler(self):
        raise NotImplementedError

    def vin_handler(self):
        raise NotImplementedError


class FileEdExt(FileEditor):
    def write_handler(self):
        if self.binary_file is not None:
            filename = filedialog.asksaveasfilename(filetypes=(("Binary file", "*.bin"), ("All files", "*.*")))
            if len(filename) > 0:
                try:
                    self.binary_file.write_to_file(filename)
                except IOError as e:
                    print('IOError:', e)
                else:
                    self.set_message('Write OK!')

    def print_handler(self):
        if self.binary_file is not None:
            self.binary_file.print_file_in_hex()

    def vin_handler(self):
        if self.binary_file is not None:
            self.binary_file.put_vin(str(self.vin_text.get()))
            self.set_message(self.get_checksum_str())


ui = FileEdExt()
ui.mainloop()
