import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from os import sep
from serial.tools import list_ports
from BinaryFile import BinaryFile
from ArduinoEEPROM import ArduinoEEPROM


class TkGUI(ttk.Frame):
    def __init__(self, master=None):
        self.filename = ''
        self.binary_file = None
        self.programmer = None
        ttk.Frame.__init__(self, master)
        self.grid(padx=10, pady=10)
        self.entry_text = tk.StringVar()
        self.message = tk.StringVar()
        self.port_list = self.list_serial_ports()
        self.create_widgets()
        self.master.resizable(0, 0)
        self.master.title('EEPROM Programmer (93c56)')
        self.style = ttk.Style()
        self.style.theme_use("clam")

    def create_widgets(self):
        # Serial frame
        self.serial_frame = ttk.LabelFrame(self, text='Programmer')
        self.serial_frame.grid(row=0, column=0, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.combobox = ttk.Combobox(self.serial_frame, values=[])
        self.combobox.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)
        self.combobox.bind('<<ComboboxSelected>>', self.combobox_item_changed)
        self.refresh_handler()

        self.refresh_button = ttk.Button(self.serial_frame, text="Refresh", command=self.refresh_handler)
        self.refresh_button.grid(row=0, column=1, sticky="nsew", padx=2, pady=2)

        # Read/Write frame
        self.rw_frame = ttk.LabelFrame(self, text='EEPROM')
        self.rw_frame.grid(row=1, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.entry = ttk.Entry(self.rw_frame, textvariable=self.entry_text)
        self.entry.grid(row=1, column=0, sticky="nsew", padx=2, pady=2)

        self.browse_button = ttk.Button(self.rw_frame, text="Browse", command=self.browse_handler)
        self.browse_button.grid(row=1, column=1, sticky="nsew", padx=2, pady=2)

        self.write_button = ttk.Button(self.rw_frame, text="Write to EEPROM", command=self.write_handler)
        self.write_button.grid(row=2, column=0, columnspan=1, sticky="nsew", padx=2, pady=2)

        self.print_button = ttk.Button(self.rw_frame, text="Print to console", command=self.print_handler)
        self.print_button.grid(row=2, column=1, columnspan=1, sticky="nsew", padx=2, pady=2)

        self.read_button = ttk.Button(self.rw_frame, text="Read EEPROM to file", command=self.read_handler)
        self.read_button.grid(row=3, column=0, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.erase_button = ttk.Button(self.rw_frame, text="Erase EEPROM", command=self.erase_handler)
        self.erase_button.grid(row=4, column=0, columnspan=1, sticky="nsew", padx=2, pady=2)

        self.read_test_button = ttk.Button(self.rw_frame, text="Read test", command=self.read_test_handler)
        self.read_test_button.grid(row=4, column=1, columnspan=1, sticky="nsew", padx=2, pady=2)

        # Message frame
        self.message_frame = ttk.LabelFrame(self, text='Message')
        self.message_frame.grid(row=2, columnspan=2, sticky="nsew", padx=2, pady=2)

        self.text_field = ttk.Label(self.message_frame, textvariable=self.message)
        self.text_field.grid(sticky="nsew", padx=2, pady=2)

    def get_file(self):
        return self.binary_file

    def set_file(self, new_filename):
        try:
            self.binary_file = BinaryFile(new_filename)
        except IOError as e:
            print('IOError', e)
            self.binary_file = None
            self.set_entry_text('')
            self.set_message(str(e))
        else:
            self.set_entry_text(new_filename)
            vin_a, vin_b = self.binary_file.get_vin()
            cs_a = self.binary_file.read_checksum()
            cs_b = self.binary_file.calc_checksum()
            self.set_message('CHKSUM:%04X/%04X  VIN:%s/%s ' % (cs_a, cs_b, vin_a, vin_b))

    def set_programmer(self, portname):
        if self.programmer is None:
            self.programmer = ArduinoEEPROM(portname)
        else:
            self.programmer.set_serial_instance(portname)

    # def get_filename(self):
    #     return self.filename
    #
    # def set_filename(self, new_filename):
    #     self.filename = new_filename
    #     self.set_entry_text(self.filename)

    def set_entry_text(self, new_text):
        self.entry_text.set(new_text.split(sep)[-1])

    def set_message(self, new_message):
        if type(new_message) is not str:
            new_message = str(new_message)
        self.message.set(new_message)

    def get_combobox_item(self):
        return self.combobox.get()

    def combobox_item_changed(self, eventObject):
        self.set_programmer(self.get_combobox_item())

    def update_combobox(self, new_values):
        if type(new_values) is list:
            self.combobox['values'] = new_values
        else:
            print('ERROR: Combobox values must be in a list')

    @staticmethod
    def list_serial_ports():
        port_list = [port.device for port in list_ports.comports()]
        return port_list

    def refresh_handler(self):
        self.port_list = self.list_serial_ports()
        self.update_combobox(self.port_list)
        if len(self.port_list) == 0:
            self.combobox.set('Select serial port')
        elif self.get_combobox_item() not in self.port_list:
            self.combobox.set(self.port_list[0])
            self.set_programmer(self.port_list[0])

    def browse_handler(self):
        filename = filedialog.askopenfilename(filetypes=(("Binary file", "*.bin"), ("All files", "*.*")))
        if len(filename) > 0:
            # self.set_filename(filename)
            self.set_file(filename)

    def write_handler(self):
        raise NotImplementedError

    def print_handler(self):
        raise NotImplementedError

    def read_handler(self):
        raise NotImplementedError

    def read_test_handler(self):
        raise NotImplementedError

    def erase_handler(self):
        raise NotImplementedError

# ui = TkGUI()
# ui.mainloop()
