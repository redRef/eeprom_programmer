from BinaryFile import BinaryFile


class BinaryFilePlus(BinaryFile):
    def brute_xor(self, look_for):
        print(f'Looking for {look_for}. Method=xor')
        # for k in range(0, 128):
        for k in range(0, 15):
            # for j in range(k, 128):
            for j in range(k, 15):
                res = self.calc_checksum(start=k * 2, end=j * 2, print_progress=False)
                if (str.format('%04X' % res).find(look_for)) >= 0:
                    print('start=%04X, end=%04X' % (k*2, j*2))
                    print(str.format('%04X' % res))
        return 0

    def brute_sum(self, look_for):
        # res = self.calc_checksum(start=0x00D, end=0x00E, print_progress=True, method='sum')
        print(f'Looking for {look_for}. Method=sum')
        for k in range(0, 15):
            for j in range(k, 15):
                res = self.calc_checksum(start=k * 2, end=j * 2, print_progress=False, method='sum')
                if look_for in f'{res:04X}':
                    print(f'start={k*2:04X}, end={j*2:04X}')
                    print(f'{res:04X}')
                    # print(f"masked result{res & 0xFFFF:04X}")
                # else:
                #     print(f'{res:04X}')

    def msb_to_lsb(self):
        print(f"Before:{self.buffer.hex()}")
        if len(self.buffer) % 2 == 0:
            result = self.buffer.copy()
            result[1::2] = self.buffer[::2]
            result[::2] = self.buffer[1::2]
            print(f"After :{result.hex()}")
            self.buffer = result
        else:
            print(f"Cannot convert odd length buffer (len={len(self.buffer)})")


# file = BinaryFilePlus('./EEPROM_dumps/ext/pioneerfxm2017bad_(YS3EH49G333026688).bin')
# # file.calc_checksum(i=0x8, end=0xFE, print_progress=True)
# # file.calc_checksum(i=0x1C, end=0xFE, print_progress=True)
# file.brute_xor(str.format('%04X'%file.read_checksum()))

# CDC checksums
# file = BinaryFilePlus('./EEPROM_dumps/dump_cdc.bin')
# file = BinaryFilePlus('./EEPROM_dumps/dump_cdc_lsb.bin')
file = BinaryFilePlus('./EEPROM_dumps/dump_cdc_modified_lsb.bin')
# file.msb_to_lsb()
# file.write_to_file('./EEPROM_dumps/dump_cdc_modified_lsb.bin')
# file = BinaryFilePlus('./EEPROM_dumps/ext/CDX_93c56_original.bin')
# file = BinaryFilePlus('./EEPROM_dumps/dump_cdc_modified.bin')
# file = BinaryFilePlus('./EEPROM_dumps/ext/CDX_93c56_original.bin')
# file = BinaryFilePlus('./EEPROM_dumps/ext/CDX_20180501_214816_EEPROM_935616bit.bin')
# file.brute_xor(look_for=f'{file.read_checksum():04X}')
# file.brute_sum(look_for=f'{file.read_checksum():04X}')
# 056019 -> 905601

