import serial
import sys
import time

timestring = time.strftime("%Y-%m-%d-%H%M%S", time.localtime())

in_file = './dump_original_unit_2018-07-15-113017.bin'
out_file = './dump_%s.bin' % timestring

VERBOSE_OUTPUT = True

ser = serial.Serial(port='/dev/ttyACM0', baudrate=115200, timeout=5)


# ser = serial.Serial(port='COM4', baudrate=115200, timeout=5)


def read_from_file(filename):
    print('Input file: %s' % filename)
    with open(filename, 'rb') as f:
        f_buffer = f.read()
        print('File size: %d B' % len(f_buffer))
        return f_buffer


def write_to_file(filename, buffer):
    print('Writing %s B to %s' % (len(buffer), filename))
    with open(out_file, 'wb') as dump_file:
        dump_file.write(buffer)


def arduino_init(timeout):
    if (VERBOSE_OUTPUT): print('Checking arduino')
    test_msg = 'OK?\n'
    resp_msg = ''
    delay = 0.1
    timeout = timeout / delay
    ser.reset_output_buffer()
    ser.reset_input_buffer()
    while ((resp_msg != test_msg) and (timeout > 0)):
        ser.write(test_msg.encode())
        if (ser.in_waiting > 0):
            resp_msg = ser.readline().decode()
            # print(resp_msg)
        time.sleep(delay)
        timeout = timeout - 1
    if (timeout <= 0):
        if (VERBOSE_OUTPUT): print('Arduino timeout')
        exit(126)
    else:
        if (VERBOSE_OUTPUT): print('Arduino is ready')
    ser.reset_output_buffer()
    ser.reset_input_buffer()


def receive_data():
    print('Receiving data from Arduino')
    in_buffer = ser.read_all()
    message_len = len(in_buffer)
    if (VERBOSE_OUTPUT): print('Incoming %d B' % (message_len))
    return in_buffer


def print_pg_bar(pg_bar):
    result_list = ['['] + pg_bar + [']']
    print('', end='\r')
    sys.stdout.write(''.join(result_list))
    sys.stdout.flush()


def send_data(buffer):
    print('Sending data to Arduino')
    size_of_buf = len(buffer)
    addr = 0;
    pgb_list = ['-' for x in range(48)]
    pgb_scale = size_of_buf / len(pgb_list)
    while (addr < size_of_buf):
        if (VERBOSE_OUTPUT):
            print('#%d: %.2x%.2x' % (addr / 2, buffer[addr], buffer[addr + 1]))
        else:
            print_pg_bar(pgb_list)
            pgb_list[int(addr / pgb_scale)] = '+'
        ser.write(buffer[addr].to_bytes(1, byteorder='big'))
        ser.write(buffer[addr + 1].to_bytes(1, byteorder='big'))
        ser.flush()
        # while(ser.in_waiting < 3):
        #     time.sleep(0.01)
        # response = ser.read_all()
        response = ser.read(3)
        if (VERBOSE_OUTPUT): print('->%s' % response)
        # print('->%s' % response.decode())
        if (response != b'OK!'):
            print('Response error')
            break
        addr = addr + 2
    print('')


def read_eeprom():
    print('== Read from EEPROM ==')
    try:
        if ser.isOpen():
            time.sleep(2)
            # ser.reset_output_buffer()
            # ser.reset_input_buffer()
            ser.write(bytearray.fromhex('AA'))
            prog_resp = ser.readline()
            print(prog_resp.decode(), end='')
            print('Sending reqest')
            time.sleep(0.5)
            buffer = receive_data()
            if (len(buffer) > 0):
                write_to_file(out_file, buffer)
            else:
                print('Nothing to write')
        else:
            print('Serial unavailable')
    except Exception as e:
        print(e)


def write_eeprom():
    print('== Writing to EEPROM ==')
    if ser.isOpen():
        file_buffer = read_from_file(in_file)
        ser.write(bytearray.fromhex('DD'))
        ser.flush()
        prog_resp = ser.readline()
        print(prog_resp.decode(), end='')
        if (len(prog_resp) == 0):
            print('No response form arduino')
            exit()
        send_data(file_buffer)
        ser.reset_input_buffer()
        time.sleep(2)
        buffer = receive_data()
        if (buffer == file_buffer):
            print('Memory compare - OK!')
            ser.write(bytearray.fromhex('AD'))
            prog_resp = ser.readline()
            print(prog_resp.decode(), end='')
        else:
            print('Memory compare - FAIL')
        # if (len(buffer) > 0):
        #     write_to_file(out_file, buffer)
        # else:
        #     print('Nothing to write')
    else:
        print('Serial unavailable')


arduino_init(5)
# write_eeprom()
# print(ser.readline())
read_eeprom()
ser.close()
exit()
