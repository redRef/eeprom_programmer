import sys
from tkinter import messagebox
import TkGUI


class TkOverride(TkGUI.TkGUI):
    def write_handler(self):
        if self.binary_file:
            self.set_message('Writing EEPROM')
            if self.programmer.serial_instance is not None:
                self.set_message(self.programmer.write_handler(self.binary_file))
            else:
                self.set_message('Serial port error')
        else:
            self.set_message('Select binary file')

    def print_handler(self):
        if self.binary_file:
            self.binary_file.print_file_in_hex()
        else:
            self.set_message('Select binary file')

    def read_handler(self):
        self.set_message('Read EEPROM')
        if self.programmer.serial_instance is not None:
            # self.set_message(self.programmer.read_handler()[-4:].hex())
            # self.set_message('Last 4 bytes: ' + read_message[-4:].hex())
            flash_content = self.programmer.read_handler()
            if len(flash_content) > 0:
                self.set_message(f'Save complete. Check app directory. {flash_content[-4:].hex()}')
        else:
            self.set_message('Serial port error')

    def read_test_handler(self):
        self.set_message('Read Test')
        if self.programmer.serial_instance is not None:
            # self.set_message('Last 4 bytes:' + read_message[-4:].hex())
            self.set_message(self.programmer.read_test_handler()[-4:].hex())
        else:
            self.set_message('Serial port error')

    def erase_handler(self, check=True):
        result = messagebox.askquestion(title='Erase data from flash', message='Are you sure?')
        if result == 'yes':
            self.set_message('Erasing EEPROM')
            if self.programmer.serial_instance is not None:
                # self.set_message('Last 4 bytes:' + read_message[-4:].hex())
                self.set_message(self.programmer.erase_handler())
            else:
                self.set_message('Serial port error')
        else:
            self.set_message('Erase canceled')


ui = TkOverride()
ui.mainloop()
sys.exit()
