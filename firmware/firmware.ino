//#define AT93C46
#define AT93C56

#include "softSPI.h"

#define BAUD 115200
#define COMM_READ 0xAA
#define COMM_RECEIVE 0xDD
#define COMM_WRITE 0xAD
#define COMM_ERASE 0x7D

bool readEnable = false;
bool receiveEnable = false;
bool writeEnable = false;
bool eraseEnable = false;


void spi_enable(bool activate){
  if (activate){
    pinMode(MOSI, OUTPUT);
//    pinMode(MISO, INPUT);
    pinMode(MISO, INPUT_PULLUP);
    pinMode(CLK,OUTPUT);
    pinMode(CHIPSELECT,OUTPUT);
    digitalWrite(CHIPSELECT,LOW); //disable device
    digitalWrite(CLK,LOW);
  } else {
    pinMode(MOSI, INPUT);
    pinMode(MISO, INPUT);
    pinMode(CLK, INPUT);
    pinMode(CHIPSELECT, INPUT);
  }
  _delay_ms(75);
}

void setup()
{
  Serial.begin(BAUD);
  spi_enable(false);
  _delay_ms(75);
}


void printWord(uint16_t data)
{
  char printBuffer[8];
  sprintf(printBuffer, "%04X", data);
  Serial.print(printBuffer);
}


bool printBuffer(uint16_t buf[], const char *outputFormat)
{
  uint16_t addr = ADDR_START;
  uint16_t bufferLen = WORD_COUNT;
  while(addr<bufferLen){
    if (strcmp(outputFormat, "HEX") == 0){
      printWord(buf[addr++]);
    } else if(strcmp(outputFormat, "RAW") == 0){
      Serial.write(buf[addr]>>8);
      Serial.write(buf[addr]);
      addr++;
    }
  }
  return true;
}


uint16_t serialGetWord(){
  uint8_t qweH = 0;
  uint8_t qweL = 0;
  while(Serial.available() < 2){
    _delay_us(1);
  }
  while(Serial.available() > 1){
    qweH = Serial.read();
    qweL = Serial.read();
  }
  Serial.print("OK!");
  Serial.flush();
  return ((qweH <<8) | qweL);
}


void loop()
{
  uint16_t eepromAddr=ADDR_START;
  uint16_t wordBuffer;
  uint16_t eepromBuffer[WORD_COUNT];

  if (receiveEnable){
    spi_enable(true);
    while(eepromAddr<ADDR_END){
      eepromBuffer[eepromAddr] = serialGetWord();
      eepromAddr++;
    }
    eepromAddr = ADDR_START;
    _delay_ms(100);
    printBuffer(eepromBuffer, "RAW");
    receiveEnable = false;
    spi_enable(false);
  
  } else if (readEnable){
    spi_enable(true);
    while(eepromAddr<ADDR_END){
      wordBuffer = spi_read(WORD_SIZE,eepromAddr);
      eepromBuffer[eepromAddr] = wordBuffer;
      eepromAddr++;
      _delay_us(COMM_DELAY);
    }
    eepromAddr = ADDR_START;
    readEnable = false;
    printBuffer(eepromBuffer, "RAW");
    spi_enable(false);
  
  } else if(writeEnable){   //enter write cycle
    uint8_t fResult;
    spi_enable(true);
    digitalWrite(CHIPSELECT, 1);
    spi_send_command(EWEN, 0b11000000); //    WRITE ENABLE
    digitalWrite(CHIPSELECT, 0);
    while(eepromAddr<ADDR_END){
      fResult = spi_write(eepromBuffer[eepromAddr], eepromAddr);
      eepromAddr++;
      _delay_us(COMM_DELAY);
      if (fResult == 0){
        break;
      }
    }
    digitalWrite(CHIPSELECT, 1);
    spi_send_command(EWEN, 0b00000000); //WRITE DISABLE
    digitalWrite(CHIPSELECT, 0);
    eepromAddr = ADDR_START;
    writeEnable = false;
    spi_enable(false);
    Serial.println("Write end");
  
  } else if(eraseEnable){   //enter ERASE cycle
    spi_enable(true);
    digitalWrite(CHIPSELECT, 1);
    spi_send_command(EWEN, 0b11000000); //WRITE ENABLE
    digitalWrite(CHIPSELECT, 0);
    _delay_us(1);
    digitalWrite(CHIPSELECT, 1);
    spi_send_command(ERAL, 0b10000000); //ERASE ALL
    digitalWrite(CHIPSELECT, 0);
    eraseEnable = false;
    spi_enable(false);
    Serial.println("ERASE ALL COMPLETED ");  
    _delay_ms(10);

  } else {
    _delay_ms(10);
  }
}


void serialEvent() {
  uint16_t eepromCnt = 0;
  while (Serial.available()) {
    uint8_t inChar = Serial.read();
    if (inChar == COMM_READ) {
      readEnable = true;
      Serial.print("READ_MODE_OK\n");
    } else if(inChar == COMM_RECEIVE){
      receiveEnable = true;
      Serial.print("RECEIVE_MODE_OK\n");
    } else if(inChar == COMM_WRITE){
      writeEnable = true;
      Serial.print("WRITE_MODE_OK\n");
    } else if (inChar == COMM_ERASE){
      eraseEnable = true;
      Serial.print("ERASE_MODE_OK\n");
    } else if (inChar == 'R'){
        spi_enable(true);
        digitalWrite(CHIPSELECT,1);
        spi_send_command(READ, 0);
        while(eepromCnt<WORD_COUNT){
//          printWord(eepromCnt);
//          Serial.write('_');
          printWord(read_miso(17));
//          Serial.write('\n');
          eepromCnt++;
//          _delay_us(COMM_DELAY);
        }
        digitalWrite(CHIPSELECT,0);
        Serial.write('\n');
        spi_enable(false);
    } else{
      Serial.write(inChar);
    }
  }
}
