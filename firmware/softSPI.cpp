//#define AT93C46
#define AT93C56

#include <Arduino.h>
#include "softSPI.h"


inline void spi_clk_tick()
{
  digitalWrite(CLK,1);
  _delay_us(COMM_DELAY);
  digitalWrite(CLK,0);
  _delay_us(COMM_DELAY);
}

inline void spi_send_bit(uint8_t data){
  digitalWrite(MOSI,(1 & data));
  spi_clk_tick();
}


void spi_send_command(volatile uint8_t spiComm, uint8_t spiAddr)
{
  uint8_t i;
  _delay_us(COMM_DELAY);
  spi_send_bit(1);
  for(i=0; i<COMM_LENG; i++){
    spi_send_bit((spiComm >> (COMM_LENG-1-i)) & 1);
  }
  for(i=0; i<ADDR_LENG; i++){
    spi_send_bit((spiAddr >> (ADDR_LENG-1-i)) & 1);
  }
  if(spiComm == READ){
    digitalWrite(MOSI,0);
  }
}


uint16_t spi_read(uint16_t bitsToRead, uint8_t startAddr)
{
  uint16_t eepromOut = 0;
  uint8_t dataH, dataL;
  
  digitalWrite(CHIPSELECT,1);
  spi_send_command(READ, startAddr);
  if(DUMMY_BIT){
      bitsToRead += 1;
  }
  while(bitsToRead>0){
    eepromOut |= digitalRead(MISO);
    spi_clk_tick();
//    _delay_us(10);
    bitsToRead--;
    if (bitsToRead > 0) eepromOut = eepromOut<<1;
  }
  digitalWrite(CHIPSELECT,0);
  dataH = (eepromOut & 0xFF);
  dataL = (eepromOut >> 8);
  return (dataH << 8 | dataL);
}

uint8_t spi_write(uint16_t data, uint8_t startAddr)
{
  uint8_t dataH, dataL;
  uint8_t shift = WORD_SIZE - 1;
  
  dataH = data >> 8;
  dataL = data & 0xFF;
  data = (dataL << 8) | (dataH);
  digitalWrite(CHIPSELECT,1);
  spi_send_command(WRITE, startAddr);
  for(shift=0; shift<WORD_SIZE; shift++){
    spi_send_bit((data >> (WORD_SIZE-1-shift)) & 1);
  }
  digitalWrite(CHIPSELECT,0);
  digitalWrite(MOSI,0);
  _delay_us(1);
  digitalWrite(CHIPSELECT,1);
  uint16_t counter = 0;
  while((digitalRead(MISO) != 1) && (counter < 15)){
//    Serial.print(digitalRead(MISO));
    digitalWrite(CHIPSELECT,0);
    spi_clk_tick();
    _delay_ms(2);
    counter++;
    digitalWrite(CHIPSELECT,1);
  }
  digitalWrite(CHIPSELECT,0);
  if (counter<15){
    return 1;
    Serial.print("Write OK! ");  
  } else {
    Serial.print("Write timeout ");
    return 0;
  }
}

uint16_t read_miso(uint16_t bitsToRead){
  uint16_t eepromOut = 0;
  while(bitsToRead>0){
    eepromOut |= digitalRead(MISO);
    spi_clk_tick();
//    _delay_us(COMM_DELAY);
    bitsToRead--;
    if (bitsToRead > 0) eepromOut = eepromOut<<1;
  }
  return eepromOut;
}
