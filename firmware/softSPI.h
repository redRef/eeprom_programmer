/*softSPI.h*/
#ifndef SOFTSPI_H
  #define SOFTSPI_H
  
  #define CHIPSELECT 10
  #define MOSI 11
  #define MISO  12
  #define CLK  13
  
  //typedef struct{
  //  char * label;
  //  uint8_t wSize;
  //  uint8_t wCount;
  //  uint8_t cmdLen;
  //  uint8_t AdrLen;
  //  bool dummyBit;
  //  uint8_t cmdDelay;
  //  uint8_t AdrStar;
  //  uint8_t AdrEnd;
  //}settings;
  
  
  #ifdef AT93C56
    #define WORD_SIZE 16
    #define WORD_COUNT 128
    #define COMM_LENG 2
    #define ADDR_LENG 8
    #define DUMMY_BIT true
    #define COMM_DELAY 1
    #define ADDR_START 0
    #define ADDR_END 128
  //  Op Codes
    #define EWEN 0b00   //Write enable must precede all prog. modes. ADDR 11XXXXXX
    #define ERAL 0b00   //Writes memory location An - A0. ADDR 10XXXXXX
    #define WRITE 0b01  //Writes memory location A n - A 0 .
    #define READ  0b10  //Reads data stored in memory, at specified address.
    #define ERASE 0b11  //Erases memory location An - A0.
  #else
    #define WORD_SIZE 0
    #define WORD_COUNT 0
    #define ADDR_LENG 0
    #define DUMMY_BIT false
    #define COMM_DELAY 10
    #define ADDR_START 0
    #define ADDR_END 0
  #endif
  
  #ifdef AT93C46
    #define WORD_SIZE 16
    #define WORD_COUNT 64
    #define COMM_LENG 2
    #define ADDR_LENG 6
    #define DUMMY_BIT true
    #define COMM_DELAY 1
    #define ADDR_START 0
    #define ADDR_END 64
  //  Op Codes
    #define EWEN 0b00   //Write enable must precede all prog. modes. ADDR 11XXXXXX
    #define ERAL 0b00   //Writes memory location An - A0. ADDR 10XXXXXX
    #define WRITE 0b01  //Writes memory location A n - A 0 .
    #define READ  0b10  //Reads data stored in memory, at specified address.
    #define ERASE 0b11  //Erases memory location An - A0.
  #endif
  
  inline void spi_clk_tick();
  inline void spi_send_bit(uint8_t data);
  void spi_send_command(volatile uint8_t spiComm, uint8_t spiAddr);
  uint16_t spi_read(uint16_t bitsToRead, uint8_t startAddr);
  uint8_t spi_write(uint16_t data, uint8_t startAddr);
  uint16_t read_miso(uint16_t bitsToRead);

#endif /* !SOFTSPI_H */
